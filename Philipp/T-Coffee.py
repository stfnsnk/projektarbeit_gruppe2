import requests
from time import sleep

base_url = "https://www.ebi.ac.uk/Tools/services/rest/tcoffee"
sequencefile = "multialignments.fa"

with open(sequencefile) as seq:
    sequences = seq.read()
data = {
    "email":"fh@derauer.net",
    "format":"fasta_aln",
    "matrix":"none",
    "order":"aligned",
    "stype":"dna",
    "sequence": sequences
}
run_url = base_url + "/run"
r = requests.post(run_url, data=data)
print(r.status_code,r.reason)
job_id = r.text

status = ""
while status != "FINISHED":
    print("Job not finished yet")
    sleep(1)
    status_url=base_url+"/status/"+job_id
    s= requests.get(status_url)
    print(s.status_code,s.reason)
    status = s.text
else:
    print("Job finished")

result_url = base_url + "/result/"+job_id+"/aln-fasta"
r = requests.get(result_url)
print(r.status_code,r.reason)
result=r.text
print(result)

r=requests.get(base_url + "/resulttypes/" + job_id)
r.text