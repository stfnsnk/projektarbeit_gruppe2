import requests
from time import sleep
from pathlib import Path


#def Multialigner_Gruppe2 (tool:str,inputfile: ):
selection = None
while not selection in ["1","2","3","4"]:
    selection = input('''Please choose Alignmenttool (Enter number)
                     1 = k-align (Grace)
                     2 = MAFFT (Stefan)
                     3 = clustal-Omega (Martina)
                     4 = T-Coffee (Philipp)''')

    if selection == "1":
        tool = "k-align"
    elif selection == "2":
        tool = "MAFFT"
    elif selection == "3":
        tool = "clustal-Omega"
    elif selection == "4":
        tool = "T-Coffee"


inputfile = input("Please enter absolute Path of File")

'''
* Phase 1:
  Erstellen Sie Funktionen um die verschiedene multiplen Sequenz Alignment Tools über das EBI RestApi anzusprechen.
  Überlegen sie sich ein einheitliches Interface für die Funktionen.
  Aus folgende Tools können Sie auswählen.
   * MUSCLE
  * k-align (Grace)
  * MAFFT (Stefan)
  * clustal-Omega (Martina)
  * T-Coffee (Philipp)

To Do:
- Einlesen der Files über Absoluten Pfad lt. Userinput
- Toolspezifische Variablen finden und per If Statement auswählen
'''

if tool == "k-align":
    base_url =
    data =
    output_variable =  # letzter Teil der result URL

if tool == "MAFFT":
    base_url =
    data =
    output_variable = #letzter Teil der result URL

if tool == "clustal-Omega":
    base_url =
    data =
    output_variable =  # letzter Teil der result URL

if tool == "T-Coffee":
    base_url =
    data =
    output_variable =  # letzter Teil der result URL




def high_expectation_function_V1 (base_url,data,output_variable):
    with open(inputfile, "r") as f_in:
        sequence = f_in.read()

    get_run_url = base_url + "run/"

    r = requests.post(get_run_url, data=data)
    print(r.status_code, r.reason)
    job_id = r.text

    # Wait for finishing task
    status = ""
    while status != "FINISHED":
        print("Job not finished yet")
        sleep(1)
        get_status_url = base_url + "status/" + job_id
        r = requests.get(get_status_url)
        print(r.status_code, r.reason)
        status = r.text
    else:
        print("Job finished")

    get_result_url = f"{base_url} + /result/ + {job_id} + /{output_variable}"
    r = requests.get(get_result_url)
    print(r.status_code, r.reason)
    print(r.text)
