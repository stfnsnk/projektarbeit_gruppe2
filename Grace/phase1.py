'''
* Phase 1:
  Erstellen Sie Funktionen um die verschiedene multiplen Sequenz Alignment Tools über das EBI RestApi anzusprechen.
  Überlegen sie sich ein einheitliches Interface für die Funktionen.
  Aus folgende Tools können Sie auswählen.
   * MUSCLE
  * k-align

'''
import requests
import os
import sys
from time import sleep


##k-align NOT a pairwise alignment tool
#baseurl k-align
##possible input file formats GCG, FASTA, EMBL (Nucleotide only), GenBank, PIR, NBRF, PHYLIP or UniProtKB/Swiss-Prot
##posible output file formats fasta, clu, mascim
# https://raw.githubusercontent.com/ebi-wp/webservice-clients/master/python/kalign.py


# searches for files with certain endings
from glob import glob
for filename in glob('.fa'):
    with open(filename) as f_in:
        sequence = f_in.read()

#data zum Vereinheitlichen noch umändern
data = {
    "sequence": sequence,
    "format": "fasta",
    "tree": "none",
    "email": "fh@derauer.net",
}
base_url = 'http://www.ebi.ac.uk/Tools/services/rest/kalign'

get_run_url = base_url + "run/"
print(get_run_url)

r = requests.post(get_run_url, data=data)
print(r.status_code, r.reason)
job_id = r.text
print(job_id)

# Wait for finishing task
status = ""
while status != "FINISHED":
    print("Job not finished yet")
    sleep(1)
    get_status_url = base_url + "status/" + job_id
    r = requests.get(get_status_url)
    print(r.status_code, r.reason)
    status = r.text
else:
    print("Job finished")

get_result_url = base_url + "resulttypes/" + job_id
r = requests.get(get_result_url)
print(r.status_code, r.reason)
print(r.text)

get_aln_url = base_url + f"result/{job_id}/aln-fasta"
r = requests.get(get_aln_url)
print(r.status_code, r.reason)
print(r.text)

r = requests.options(get_run_url)
print(r.status_code, r.reason)
print(r.text)
