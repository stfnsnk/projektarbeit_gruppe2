# Projektarbeit_Gruppe2

Projektarbeit
* Phase 1:
  Erstellen Sie Funktionen um die verschiedene multiplen Sequenz Alignment Tools über das EBI RestApi anzusprechen.
  Überlegen sie sich ein einheitliches Interface für die Funktionen.
  Aus folgende Tools können Sie auswählen.
  * Clustal Omega
  * MAFFT
  * MUSCLE
  * k-align (Grace)
  * T-Coffee
* Phase 2:
  Erstellen Sie Funktionen, um die verschiedene multiplen Sequenz Alignment Tools auch über eine lokale Installation anzusprechen.
  Verwenden Sie folgende Tools:
  * MAFFT
  * MUSCLE

