import requests
import os



base_url = "https://www.ebi.ac.uk/Tools/services/rest/mafft/"

with open("multialignments_mafft.fa", "r") as f_in:
    sequence = f_in.read()

data = {
    "format": "fasta",
    "matrix": None,
    "gapopen": None,
    "gapext": None,
    "order": None,
    "nbtree": None,
    "treeout": None,
    "maxiterate": None,
    "ffts": None,
    "stype": None,
    "sequence": sequence,
    "email": "fh@derauer.net"
}

get_run_url = base_url + "run/"
print(get_run_url)

r = requests.post(get_run_url, data=data)
print(r.status_code, r.reason)
job_id = r.text


# Wait for finishing task
status = ""
while status != "FINISHED":
    print("Job not finished yet")
    sleep(1)
    get_status_url = base_url + "status/" + job_id
    r = requests.get(get_status_url)
    print(r.status_code, r.reason)
    status = r.text
else:
    print("Job finished")

get_result_url = base_url + "/result/" + job_id + "/out"
r = requests.get(get_result_url)
print(r.status_code, r.reason)
print(r.text)
alignment = r.text